Run with Python 3:

$ python program.py

Or alternatively build and run the Singularity container:

$ sudo singularity build blackjack.sif blackjack.def

$ ./blackjack.sif
